package com.example.excel.ser;

import com.example.excel.model.CacheEntry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CacheEntryRepository extends JpaRepository<CacheEntry, String> {
}
