package com.example.excel.ser;

import com.example.excel.model.CacheEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CacheService {
    @Autowired
    private CacheEntryRepository cacheRepository;

    public void saveToCache(String key, String value) {
        CacheEntry cacheEntry = new CacheEntry();
        cacheEntry.setKey(key);
        cacheEntry.setValue(value);
        cacheEntry.setTimestamp(LocalDateTime.now());
        cacheRepository.save(cacheEntry);
    }

    public String getFromCache(String key) {
        return cacheRepository.findById(key).map(CacheEntry::getValue).orElse(null);
    }

    public void evictCache(String key) {
        cacheRepository.deleteById(key);
    }


}
