package com.example.excel.ser;

import com.example.excel.model.MyData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class MyDataService {
    @Autowired
    private MyDataRepository dataRepository;
    @Autowired
    private CacheService cacheService;

    public MyData getData(Long id) {
        String cacheKey = "myData_" + id;
        String cachedData = cacheService.getFromCache(cacheKey);
        if (cachedData == null) {
            simulateDelay();
        }

        MyData data = dataRepository.findById(id).orElse(null);
        if (data != null) {
            cacheService.saveToCache(cacheKey, data.toString());
        }
        return data;
    }

    public void updateData(MyData data) {
        dataRepository.save(data);
        String cacheKey = "myData_" + data.getId();
        cacheService.evictCache(cacheKey);
    }


    private void simulateDelay() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}

