package com.example.excel.controller;



import com.example.excel.model.MyData;
import com.example.excel.ser.MyDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mydata")
public class MyDataController {

    @Autowired
    private MyDataService myDataService;

    @GetMapping("/{id}")
    public MyData getData(@PathVariable Long id) {
        return myDataService.getData(id);
    }

    @PutMapping("/{id}")
    public void updateData(@PathVariable Long id, @RequestBody MyData data) {
        data.setId(id);
        myDataService.updateData(data);
    }


}

